<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dev_conclusion".
 *
 * @property integer $id_conclusion
 * @property integer $id_deviation
 * @property integer $id_decision
 * @property string $comment
 * @property string $final_decision
 * @property integer $id_signature
 */
class DevConclusion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dev_conclusion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_deviation', 'id_decision', 'comment', 'final_decision', 'id_signature'], 'required'],
            [['id_deviation', 'id_decision', 'id_signature'], 'integer'],
            [['comment', 'final_decision'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_conclusion' => 'Id Conclusion',
            'id_deviation' => 'Id Deviation',
            'id_decision' => 'Id Decision',
            'comment' => 'Comment',
            'final_decision' => 'Final Decision',
            'id_signature' => 'Id Signature',
        ];
    }
}
