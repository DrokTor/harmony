<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dev_deviation".
 *
 * @property integer $id_deviation
 * @property integer $state
 * @property integer $creator
 * @property string $creation_date
 * @property string $creation_time
 */
class DevDeviation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dev_deviation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['state', 'creator', 'creation_date', 'creation_time'], 'required'],
            [['state', 'creator'], 'integer'],
            [['creation_date'], 'string', 'max' => 10],
            [['creation_time'], 'string', 'max' => 5]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_deviation' => 'Id Deviation',
            'state' => 'State',
            'creator' => 'Creator',
            'creation_date' => 'Creation Date',
            'creation_time' => 'Creation Time',
        ];
    }
}
