<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DevDeviation */

$this->title = 'Update Dev Deviation: ' . ' ' . $model->id_deviation;
$this->params['breadcrumbs'][] = ['label' => 'Dev Deviations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_deviation, 'url' => ['view', 'id' => $model->id_deviation]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="dev-deviation-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
